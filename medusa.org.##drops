#+BEGIN_COMMENT
.. title: Medusa. The Mother of all Hydras
.. description: My personal Hydras for Emacs 
.. slug: Medusa
.. date: 2021-07-05 11:00:00 UTC+02:00
.. tags: emacs, medusa
#+END_COMMENT

I like my Hydras. They are very useful for all the commands, that I don't use on a daily basis. I tend to forget the name of those commands, and also the keys I have bound them to. So for all these cases it's quite handy to have a small menu popping up, with hints for all those keys, and their commands. I can visually group them, and sort them in any order that makes sense to me.

What you read here is my actual configuration file, it is done via /literal configuration/ a method similar to [[https://en.wikipedia.org/wiki/Literate_programming][literate programming]] where my explanations are woven into snippets of actual configuration lines. Those snippets are tangled together and then executed by Emacs' Org-Babel package. You can also download it from [[https://codeberg.org/dr.ops/medusa][my repository]] at [[https://codeberg.org/explore/repos][Codeberg.org]]

In principle, I have *two* main entry points into my tree of hydras.

 - //medusa/major/ for all sort of things, that are tied to a special major mode.
   
   Each major mode has its own base hydra, so //medusa/major/ isn't a hydra itself, but a function, that calls the body of a hydra dependent on the active major mode This covers mostly things that are also reachable via the C-c key-prefix.
 - //medusa/emacs/body/ for all sort of things, that are not tied to a special major mode
   This covers most functions that are also reachable via the meno or C-x key-prefix
   
I tied this hydra to my /menu/ key, or the /app/ key on a windows system. Those /XF86.../ keys are in use on my special POS keyboard, that I had in use for a while.

#+BEGIN_SRC emacs-lisp :tangle yes :results silent
  (global-set-key (kbd "<menu>") #'major-mode-hydra)
  (global-set-key (kbd "<apps>") 'medusa/major)
  (global-set-key (kbd "<XF86Launch7>") 'medusa/major)
#+END_SRC

The right /Windows/ key, to the left of the Menu key is now my /super/ modifier key. Pressing it together with menu, shows me the global Emacs hydra:

#+BEGIN_SRC emacs-lisp :tangle yes :results silent
  (global-set-key (kbd "s-<menu>") 'medusa/emacs/body)
  (global-set-key (kbd "<XF86AudioNext>") 'medusa/emacs/body)
#+END_SRC


* Emacs-Hydra

  This is the entry point to my global Emacs-Hydra. Its pretty raw by now. the keys that are visible in the Menu are functional. Those keys that call functions, or other hydras, but are not visible in the menu are considered tests.
  
#+BEGIN_SRC emacs-lisp :tangle yes :results silent
	(defhydra medusa/emacs (:color blue)
	"
	=^=^==============================================================================
	=^ ^                                   Emacs                                     = 
	=^=^==============================================================================
	 _a_bbrevs _m_inor modes  _y_ankpad  _p_rojectile
	=^=^==============================================================================
	"
	 ("y" medusa/yankpad/body)
	 ("p" medusa/projectile/body)
	 ("o" org-mode)
	 ("f" medusa/file/body "File")
	 ("a" medusa/abbrevs/body)
	 ("m" medusa/minormodes/body)
	 ("<menu>" medusa/launcher)

	;; ("c" medusa/org-clock/body)
	;; ("b" org-iswitchb )
	;; ("r" medusa/org-refile-hydra/body)
	)
#+END_SRC
** Abbrevs
	 A defined “abbrev” is a word which “expands”, if you insert it, into some different text.
	Abbrevs are defined by the user to expand in specific ways.  For example, you might define ‘foo’
	as an abbrev expanding to ‘find outer otter’.  Then you could insert ‘find outer otter ’ into the
	buffer by typing ‘f o o <SPC>’.

	This hydra helps me in defining and editing those abbreviations, as I could never remember, which way of defining is the /inverse/ direction.

	It also shows two nice hydra heads, that I try to use wherever possible. The /C/ and the /?/ head. The first takes me to the Customization group for everything about abbrevs, and the latter directly into the respective section in the Emacs manual that comes with the Emcas info system.
	

#+BEGIN_SRC emacs-lisp :tangle yes :results silent
  (defhydra medusa/abbrevs (:color blue :hint nil)
  "
  -^-^------------------^-^-----------------^-^---------------------------------^-^-----
  -^ ^                  ^ ^           Abbrev^s^                                 ^ ^    - 
  -^-^------------------^-^-----------------^-^---------------------------------^-^-----
   ^^GLOBAL ABBREV     ^^MODE SPECIFIC      ^^Other                          _C_ustomize
   ^*^***************  ^*^***************   ^*^*****
   from _s_cratch      from _S_cratch       _-_ Prefix abbrev
   from _a_bbrev       from _A_bbrev        e_x_pand abbrev
   from _d_efinition   from _D_efinition    ^ ^
   ^ ^                  ^ ^                 ^ ^                                 _?_ help
  -^-^------------------^-^-----------------^-^---------------------------------^-^-----
  "
   ("-" abbrev-prefix-mark)
   ("x" expand-abbrev)
   ("l" list-abbrevs)
   ("e" edit-abbrevs)
   ("s" define-global-abbrev)
   ("S" define-mode-abbrev)
   ("d" add-global-abbrev)
   ("D" add-mode-abbrev)
   ("a" inverse-add-global-abbrev)
   ("A" inverse-add-mode-abbrev)
   ("C" (customize-group-other-window "abbrev"))
   ("?" (info "(emacs) Abbrevs"))
   ("<menu>" medusa/launcher)
  )
#+END_SRC

** Minor Modes

* General

#+BEGIN_SRC emacs-lisp :tangle yes :results silent
	(global-set-key (kbd "<find>") 'medusa/general/body)

	(pretty-hydra-define medusa/general
	  (:color blue :quit-key "<escape>" :title "Medusa - Mother of Hydras")
	  ("Basic"
	   (
		("y" medusa/yankpad/body)
		("o" org-mode)
		("p" medusa/projectile/body "Projectile")
		("f" medusa/file/body "File")
		("m" medusa/minor-modes/body "Minor Modes")
		("<menu>" medusa/launcher)
		)))
#+END_SRC
  

** File-Hydra
   
#+BEGIN_SRC emacs-lisp :tangle yes :results silent

  (defhydra medusa/file (:color blue :hint nil)
 "
 --------------------------------------------------------------------------------
 -                                  File                                        -
 --------------------------------------------------------------------------------
  _n_ew                                 _d_irectory
  _o_pen   (*)                          _p_rint
  _l_ast visited
  ^ ^ 
  _s_ave             
  _s_ave as          
  _c_lose            
  ^ ^ 
  _i_nsert           
  _r_evert           
  --------------------------------------------------------------------------------
 "
	("f" find-file-existing  :exit t)
	("n" find-file           :color blue)
	("o" find-file-existing  :color blue)
	("l" recentf-open-files  :color blue)
	("i" insert-file         :color blue)
	("r" revert-buffer       :color blue)
	("s" save-buffer         :color blue)
	("a" write-file          :color blue)
	("c" kill-this-buffer    :color blue)
	("d" dired               :color blue)
	("p" print-buffer        :color blue)
	)
#+END_SRC

** Yankpad-Hydra
   
 #+BEGIN_SRC emacs-lisp :tangle yes :results silent

(defhydra medusa/yankpad (:color blue :hint nil)
"
 --------------------------------------------------------------------------------
 -                                  Yankpad                                     -
 --------------------------------------------------------------------------------
  _i_nsert snippet                         _r_eload                                
  e_x_pand snippet                         ^ ^                                     
  _e_dit   snippets                        ^ ^                                     
  _._ repeat                               ^ ^                                     
  ^ ^                                      ^ ^                                     
  _s_et category                           ^ ^                                     
  _a_dd category                           ^ ^                                     
  ^ ^                                      ^ ^                                     
  _c_apture snippet                        ^ ^                                     
 --------------------------------------------------------------------------------
"
 ("e" yankpad-edit)
 ("i" yankpad-insert)
 ("y" yankpad-insert)
 ("c" yankpad-capture-snippet)
 ("a" yankpad-append-category)
 ("s" yankpad-set-category)
 ("r" yankpad-reload)
 ("x" yankpad-expand)
 ("." yankpad-repeat)
)
#+END_SRC

* Major Modes
#+BEGIN_SRC emacs-lisp :tangle yes :results silent
  (require 'major-mode-hydra)

  (setq major-mode-hydra-title-generator
	  '(lambda (mode)
		 (s-concat "\n"
				   (s-repeat 10 " ")
				   (all-the-icons-icon-for-mode mode :v-adjust 0.05)
				   " "
				   (symbol-name mode)
				   " commands")))
#+END_SRC

** iBuffer
#+BEGIN_SRC emacs-lisp :tangle yes :results silent

  (major-mode-hydra-define ibuffer-mode nil
		("Sort"
		 (
		  ("sa"	ibuffer-do-sort-by-alphabetic "alphabetic")
		  ("sn"	ibuffer-do-sort-by-filename/process "name")
		  ("sm"	ibuffer-do-sort-by-major-mode "major-mode")
		  ("ss"	ibuffer-do-sort-by-size "size")
		  ("sr"	ibuffer-do-sort-by-recency "recency")
		  ("<down>" ibuffer-forward-line :color red)
		  ("<up>" ibuffer-backward-line :color red)

		  )
		"Filter"
		 (
		  ("fm" ibuffer-filter-by-used-mode "mode")
		  ("fM" ibuffer-filter-by-derived-mode "derived mode")
		  ("fn" ibuffer-filter-by-name "name")
		  ("fN" ibuffer-filter-by-filename "filename")
		  ("fc" ibuffer-filter-by-content "content")
		  ("fe" ibuffer-filter-by-predicate "predicate")
		  ("fs" ibuffer-filter-by-size-lt "size")
		  ("ff" ibuffer-filter-disable "disable")
		  )
		"Mark"
		 (
		  ("mn" ibuffer-unmark-all "mark none")
		  ("mm" ibuffer-mark-by-mode "mode")
		  ("mM" ibuffer-mark-modified-buffers "modified")
		  ("mu" ibuffer-mark-unsaved-buffers "unsaved")
		  ("ms" ibuffer-mark-special-buffers "special")
		  ("mr" ibuffer-mark-read-only-buffers "read-only")
		  ("md" ibuffer-mark-dired-buffers "dired")
		  ("me" ibuffer-mark-dissociated-buffers "dissociated")
		  ("mh" ibuffer-mark-help-buffers "help")
		  ("mz" ibuffer-mark-compressed-file-buffers "compressed")
		  )
		"Action"
		 (
		  ("e" ibuffer-do-eval "eval")
		  ("E" ibuffer-do-view-and-eval "view-and-eval")
		  ("F" ibuffer-do-shell-command-file "shell-command-file")
		  ("h" ibuffer-do-query-replace "query-replace")
		  ("H" ibuffer-do-query-replace-regexp "query-replace-regexp")
		  ("M" ibuffer-do-toggle-modified "toggle-modified")
		  ("N" ibuffer-do-shell-command-pipe-replace "shell-cmd-pipe-replace")
		  ("o" ibuffer-do-occur "occur")
		  ("p" ibuffer-do-print "print")
		  ("r" ibuffer-do-revert "revert")
		  ("R" ibuffer-do-toggle-read-only "toggle-read-only")
		  ("v" ibuffer-do-view "view")
		  ("V" ibuffer-do-view-other-frame "view-other-frame")
		  ("x" ibuffer-do-shell-command-pipe "shell-command-pipe")
		)))


#+END_SRC
** Ledger

#+BEGIN_SRC emacs-lisp :tangle yes :results silent
  (major-mode-hydra-define ledger-mode nil
	("Transaction" 
	 (("<menu>" ledger-add-transaction "":color blue)
	  ("a" ledger-add-transaction "add")
	  ("c" ledger-copy-transaction-at-point "copy")
	  ("d" ledger-delete-current-transaction "delete") 
	  ("t" ledger-toggle-current-transaction "toggle")
	  ("u" ledger-schedule-upcoming "upcoming"))
	 "Occur"
	 (("o" ledger-occur "occur") 	
	  ("O" ledger-occur-refresh "occur refresh"))
	 "Navigation"
	 (("<tab>" ledger-magic-tab "Tab") 	
	  ("<up>" ledger-navigate-beginning-of-xact "up" :color red)
	  ("<down>" ledger-navigate-end-of-xact "down" :color red) 	
	  ("<C-up>" ledger-navigate-prev-xact-or-directive "x-up" :color red)
	  ("<C-down>" ledger-navigate-next-xact-or-directive "x-down" :color red))
	 "Buffer"
	 (("C" ledger-mode-clean-buffer "clean")
	  ("S" ledger-sort-buffer "sort buffer") 	
	  ("s" ledger-sort-region "sort region"))
	 "Report"
	 (("r" (ledger-report "Bestand Giro" nil) "report"))
	 ))
#+END_SRC

*** Ledger-Report

	A hydra for fast switching between ledger-reports. Basically they all run (ledger-report "<reportname>" nil) 
	
 #+BEGIN_SRC emacs-lisp :tangle yes :results silent
   (major-mode-hydra-define ledger-report-mode nil
		(
		 "Bestand"
		 (("o" (ledger-report "Bestand" nil) "Overview") 
		  ("g" (ledger-report "Bestand Giro" nil) "Giro")
		  ("b" (ledger-report "Bestand Bargeld" nil) "Bargeld")
		  ("k" (ledger-report "Bestand Kreditkarte" nil) "Kreditkarte")
		  ("h" (ledger-report "Bestand Bausparen" nil) "Bausparen")
		  )
		 "Verlauf"
		 (("O" (ledger-report "Verlauf" nil) "Overview")
		  ("G" (ledger-report "Verlauf Girokonto" nil) "Girokonto")
		  ("B" (ledger-report "Verlauf Bargeld" nil) "Bargeld")
		  ("K" (ledger-report "Verlauf Kreditkarte" nil) "Kreditkarte")
		  ("H" (ledger-report "Verlauf Bausparen" nil) "Bausparen")
		  )
		 "Freies Gehalt"
		 (("f" (ledger-report "Freies Gehalt" nil) "Freies Gehalt")
		  ("p" (ledger-report "Privatschulden" nil) "Privatschulden")
		  )
		 "Steuern"
		 (("1" (ledger-report "Steuer-Telefonrechnungen" nil) "Telefonrechnungen")
		  ("2" (ledger-report "Steuer-Haftpflicht" nil) "Haftpflicht")
		  ("3" (ledger-report "Steuer-KFZ-Haftpflicht" nil) "KFZ-Haftpflicht")
		  ("4" (ledger-report "Steuer-Kaminkehrer" nil) "Kaminkehrer")
		  ("5" (ledger-report "Steuer-Lebensversicherung" nil) "Lebensversicherung")
		  ("6" (ledger-report "Steuer-Unfallversicherung" nil) "Unfallversicherung")
		  ("7" (ledger-report "Steuer-Versicherungen" nil) "Versicherungen")
		  ("8" (ledger-report "Steuer-Unfallversicherung" nil) "Unfallversicherung")
		  ("9" (ledger-report "Steuer-Rente" nil) "Rente"))
		 ))
 #+END_SRC
 
** Org
   
#+BEGIN_SRC emacs-lisp :tangle yes :results silent
  (major-mode-hydra-define org-mode (:color blue)
	("Org" 
	 (("<menu>" org-ctrl-c-ctrl-c "Magic" :color blue )
	  ("c" medusa/org-clock/body "clock" :exit t)
	  ("b" medusa/org-block/body "block" :exit t)
	  ("<menu>" org-ctrl-c-ctrl-c "Magic" :color blue)
	  ("r" medusa/org-refile-hydra/body "refile")
	  ("y" medusa/yankpad-hydra/body "yankpad")
	  ("t" medusa/org-timer/body "Org-Timer")
	  ("f" medusa/file/body "File")
	  ("SPC" medusa/general/body "General")
	  )
	 ))
#+END_SRC

*** Org-Timer-Hydra
   
  #+BEGIN_SRC emacs-lisp :tangle yes :results silent

  (defhydra medusa/org-timer (:color blue :hint nil)
 "
  --------------------------------------------------------------------------------
  -                                  Timer                                       -
  --------------------------------------------------------------------------------
   _t_imer insert                           _g_o                                 
   _i_tem insert                            _s_top                               
   ^ ^                                      _p_ause                              
   ^ ^                                      ^ ^                                  
   ^ ^                                      _c_ountdown                          
   ^ ^                                      ^ ^                                  
   ^ ^                                      _?_ show remaining                   
  --------------------------------------------------------------------------------
 "
	("t" org-timer "insert timer" :exit t)
	("g" org-timer-start "Go" :color blue)
	("s" org-timer-stop "Stop" :color blue)
	("p" org-timer-pause-or-continue "Pause" :color blue)
	("i" org-timer-item "insert timer item" :color blue)
	("c" org-timer-set-timer "Countdown" :color blue)
	("." org-timer "insert timer" :color blue)
	("-" org-timer-item "insert timer item" :color blue)
	("," org-timer-item "insert timer item" :color blue)
	("?" org-timer-show-remaining "remaining?" :color blue)
	)
  #+END_SRC


*** Table
#+BEGIN_SRC emacs-lisp :tangle yes :results silent
(defhydra medusa/org-table (:color blue)
  "table:"
  ("c" hydra-org-clock/body "clock" :exit t)
  ("b" org-iswitchb "buffer" :color blue)
  ("<menu>" speedbar "speedbar" :color blue)
  ("r" medusa/org-refile-hydra/body "refile")
  ("y" medusa/yankpad-hydra/body "yankpad")
  )

#+END_SRC

#+BEGIN_SRC emacs-lisp :tangle yes :results silent

(defhydra medusa/org-link (:color blue)
  "link:"
  ("c" hydra-org-clock/body "clock" :exit t)
  ("b" org-iswitchb "buffer" :color blue)
  ("<menu>" speedbar "speedbar" :color blue)
  ("r" medusa/org-refile-hydra/body "refile")
  ("y" medusa/yankpad-hydra/body "yankpad")
  )

#+END_SRC

*** Babel

#+BEGIN_SRC emacs-lisp :tangle yes :results silent
  
  (defhydra medusa/babel (:color blue)
	"Source:"
	("c" hydra-org-clock/body "clock" :exit t)
	("b" org-iswitchb "buffer" :color blue)
	("<menu>" org-ctrl-c-ctrl-c "evaluate" :color blue)
	("<apps>" org-ctrl-c-ctrl-c "evaluate" :color blue)
	("r" medusa/org-refile-hydra/body "refile")
	("y" medusa/yankpad-hydra/body "yankpad")

  
	("SPC" medusa/general/body "General")
	)
  
#+END_SRC

*** Block-Hydra 

  #+BEGIN_SRC emacs-lisp :tangle yes :results silent
(pretty-hydra-define medusa/org-block 
  (:color blue :quit-key "<escape>" :title "Org Block")
  ("Create"
   (
	   ("q" (org-insert-structure-template "quote") "quote")
	   ("v" (org-insert-structure-template "verse") "verse")
	   ("e" (org-insert-structure-template "example") "example")
  

	   ("s" (org-insert-structure-template "src" ) "src" )
	   ("o" org-insert-structure-template "Other" )
	)
   "Export"
  (
	   ("E" (org-insert-structure-template "export") "Export")
	   ("a" (org-insert-structure-template "export ascii") "Ascii")
	   ("h" (org-insert-structure-template "export html") "Html")
	   ("l" (org-insert-structure-template "export latex") "Latex")
   
	)
   "Other"
   (
   ("?" (org-info "Blocks") "Help")
 	)
   "Help"
   (
    ("-" org-babel-demarcate-block "Split")
	)
   ))
 #+END_SRC
 
 #+BEGIN_SRC emacs-lisp :tangle yes :results silent
#+END_SRC

*** Clocking-Hydra 
#+BEGIN_SRC emacs-lisp :tangle yes :results silent
  (global-set-key (kbd "C-<f11>") 'medusa/org-clock/body)
  (defhydra medusa/org-clock (:color blue)
	 "
  ^Clock:^ ^In/out^     ^Edit^   ^Summary^    | ^Timers:^ ^Run^           ^Insert
  -^-^-----^-^----------^-^------^-^----------|--^-^------^-^-------------^------
  (_?_)    _i_n         _e_dit   _g_oto entry | (_z_)     _b_egin      ti_m_e
   ^ ^     _c_ontinue   _q_uit   _d_isplay    |  ^ ^      cou_n_tdown     i_t_em
   ^ ^     _o_ut        ^ ^      _r_eport     |  ^ ^      _p_ause toggle
   ^ ^     ^ ^          ^ ^      ^ ^          |  ^ ^      _s_top
  "
	 ("i" org-clock-in)
	 ("c" org-clock-in-last)
	 ("o" org-clock-out)
   
	 ("e" org-clock-modify-effort-estimate)
	 ("q" org-clock-cancel)

	 ("g" org-clock-goto)
	 ("d" org-clock-display )
	 ("r" org-clock-report)
	 ("?" (org-info "Clocking commands"))

	("b" org-timer-start)
	("n" org-timer-set-timer)
	("p" org-timer-pause-or-continue)
	("s" org-timer-stop)

	("m" org-timer)
	("t" org-timer-item)
	("z" (org-info "Timers"))
)
  #+END_SRC

** Old version
#+BEGIN_SRC emacs-lisp :tangle yes :results silent

	  (defun medusa/major ()
			"A launcher for hydras based on the current context."
			(interactive)
			(cl-case major-mode
			  ('org-mode (let* ((elem (org-element-context))
								(etype (car elem))
								(type (org-element-property :type elem)))
						   (cl-case etype
							 (src-block (medusa/babel/body))
							 (link (medusa/org-link/body))
							 ((table-row table-cell) (medusa/org-table/body) )
							 (headline (medusa/org/body)) ; still to define
							 (paragraph (medusa/org/body)) ; still to define
							 (quote-block (medusa/org-block/body)) ; still to define
							 (export-block (medusa/org-block/body)) ; still to define
							 (verse-block (medusa/org-block/body)) ; still to define
							 (example-block (medusa/org-block/body)) ; still to define
							 (comment-block (medusa/org-block/body)) ; still to define
							 (t (medusa/org/body) (message "No hydra for this type: %s " etype))
						   )
						  )
						 )
			  ('ledger-mode (medusa/ledger/body))
			  ('ledger-report-mode (medusa/ledger-report/body))
			  (t (medusa/general/body) (message "No hydra for this major mode: %s" major-mode))))
#+END_SRC

* Minor Modes
	 A minor mode is an optional editing mode that alters the behavior of Emacs in some well-defined
	 way.  Unlike major modes, any number of minor modes can be in effect at any time.  Some minor
	 modes are “buffer-local”, and can be turned on (enabled) in certain buffers and off (disabled) in
	 others.  Other minor modes are “global”: while enabled, they affect everything you do in the
	 Emacs session, in all buffers.  Most minor modes are disabled by default, but a few are enabled
	 by default.

#+BEGIN_SRC emacs-lisp :tangle yes :results silent

	(pretty-hydra-define medusa/minor-modes
	  (:color amaranth :quit-key "<escape>" :title "Minor-Modes")
	  ("Basic"
	   (("p" projectile-mode "Projectile" :toggle t)
		("F" flycheck-mode :toggle t))
		"Numbering"
	   ( ("c" column-number-mode "column-number" :toggle t)
		 ("l" line-number-mode "line-number" :toggle t)
		 ("L" display-line-numbers-mode "display-line-numbers" :toggle t)
		 ("i" icomplete-mode :toggle t))
		"Appearance"
	   ( ("m" menu-bar-mode :toggle t)
		 ("t" tool-bar-mode :toggle t)
		 ("s" scroll-bar-mode :toggle t)
		 ("TB" tab-bar-mode :toggle t)
		 ("TL" tab-line-mode :toggle t)
		 ("a" abbrev-mode :toggle t)
		 ("w" visual-line-mode "wrap lines" :toggle t)
		 ("S" auto-save-mode :toggle t)
		 ("f" auto-fill-mode :toggle t)
		 ("?" (info "(emacs) Minor Modes") ))))
#+END_SRC

** Projectile
#+BEGIN_SRC emacs-lisp :tangle yes :results silent
(pretty-hydra-define medusa/projectile
  (:color blue :quit-key "<escape>" :title "Projectile")
  ("Project"
   (
	("c"  projectile-configure-project "configure")
	("p"	projectile-switch-project "switch project")
	("k"	projectile-kill-buffers "kill buffers")
	("s"	projectile-save-project-buffers "save buffers")
	("l"	projectile-edit-dir-locals "edit dir locals")
	)
   "File"
   (
	("o"	projectile-find-file-dwim "open")
	("O"	projectile-recentf "open recent")
	("d"	projectile-dired "dired")
	("b"	projectile-ibuffer "buffer")
	("t"	projectile-find-test-file)
	("V"	projectile-browse-dirty-projects)
	("a"	projectile-find-other-file)
	)
   "Run"
   (
	("R"	projectile-run-project)
	("C"	projectile-compile-project)
	("P"	projectile-package-project)
	("L"	projectile-install-project)
	("T"	projectile-test-project)
	("E"	projectile-regenerate-tags)
	("-"	projectile-toggle-between-implementation-and-test)
	)
   "Find"
   (
	("ft"	projectile-find-tag)
	("fo"	projectile-multi-occur)
	("fr"	projectile-replace)
	("fg"	projectile-ag)
	("<left>"	projectile-previous-project-buffer :color red)
	("<right>"	projectile-next-project-buffer :color red)
	)
   ))

#+END_SRC


